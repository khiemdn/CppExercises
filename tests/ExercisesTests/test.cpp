#define BOOST_TEST_MODULE ExercisesTest

#include <boost/test/included/unit_test.hpp>
#include "../../src/Exercises/Exercise1.h"
#include "../../src/Exercises/Exercise2.h"
#include "../../src/Exercises/Exercise3.h"
#include "../../src/Exercises/Exercise4.h"
#include "../../src/Exercises/Exercise5.h"
#include "../../src/Exercises/Exercise6.h"
#include "../../src/Exercises/Exercise7.h"
#include "../../src/Exercises/Exercise8.h"
#include "algorithm"

using namespace std;

#pragma region exercise1_test
BOOST_AUTO_TEST_SUITE(exercise1)

BOOST_AUTO_TEST_CASE(merge_and_sort_on_1_empty_vector)
{
	vector<int> v1 = { 5, 2, 3, 1, 0 };
	vector<int> v2 = {};
	vector<int> v = MergeAndSort(v1, v2);
	vector<int> expect = {0,1,2,3,5};
	BOOST_TEST(v == expect, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(merge_and_sort_on_2_empty_vectors)
{
	vector<int> v1 = {};
	vector<int> v2 = {};
	vector<int> v = MergeAndSort(v1, v2);
	vector<int> expect = {};
	BOOST_TEST(v == expect, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(merge_and_sort_2_vectors)
{
	vector<int> v1 = {5, 2, 3};
	vector<int> v2 = {9, 1, 8};
	vector<int> v = MergeAndSort(v1, v2);
	vector<int> expect = { 1, 2, 3, 5, 8, 9};
	BOOST_TEST(v == expect, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(remove_even_on_empty_deque)
{
	deque<int> d = {};
	deque<int> result = RemoveEven(d);
	deque<int> expect = {};
	BOOST_TEST(result == expect, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(remove_even_on_deque)
{
	deque<int> d = { 3, 2, 1, 5, 4, 8, 6};
	deque<int> result = RemoveEven(d);
	deque<int> expect = { 3, 1, 5};
	BOOST_TEST(result == expect, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(remove_even_on_even_deque)
{
	deque<int> d = { 4, 2, 0 , 6, 12, 8 };
	deque<int> result = RemoveEven(d);
	deque<int> expect = {};
	BOOST_TEST(result == expect, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(intersection_on_1_empty_set)
{
	set<int> s1 = { 1, 2, 3 };
	set<int> s2 = {};
	set<int> result = Intersection(s1, s2);
	set<int> expect = {};
	BOOST_TEST(result == expect, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(intersection_on_2_empty_set)
{
	set<int> s1 = {};
	set<int> s2 = {};
	set<int> result = Intersection(s1, s2);
	set<int> expect = {};
	BOOST_TEST(result == expect, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(intersection_on_2_sets)
{
	set<int> s1 = { 1, 2, 3 };
	set<int> s2 = { 3, 4, 5};
	set<int> result = Intersection(s1, s2);
	set<int> expect = { 3 };
	BOOST_TEST(result == expect, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(remove_criteria_on_empty_map)
{
	map<string, int> m;
	map<string, int> result = RemoveCriteria(m, [](const pair<string, int>& e) { return e.second > 2; });
	BOOST_TEST(result.size() == 0);
}

BOOST_AUTO_TEST_CASE(remove_criteria_on_map)
{
	map<string, int> m;
	m["a"] = 2;
	m["b"] = 3;
	m["c"] = 4;
	m["d"] = 5;
	m["e"] = 6;
	map<string, int> result = RemoveCriteria(m, [](const pair<string, int>& e) { return e.second > 4; });
	BOOST_TEST(result.size() == 2);
	BOOST_TEST(result["d"] == 5);
	BOOST_TEST(result["e"] == 6);
}

BOOST_AUTO_TEST_SUITE_END()
#pragma endregion

#pragma region exercise2_test
BOOST_AUTO_TEST_SUITE(exercise2)

BOOST_AUTO_TEST_CASE(prime_sieve_generate_3_numbers)
{
	PrimeSieve* p = new PrimeSieve(100);

	BOOST_TEST(p->GetNextPrime() == 2);
	BOOST_TEST(p->GetNextPrime() == 3);
	BOOST_TEST(p->GetNextPrime() == 5);
}

BOOST_AUTO_TEST_CASE(prime_sieve_check_100th_number)
{
	PrimeSieve* p = new PrimeSieve(1000);

	for (int i = 0; i < 99; i++)
	{
		p->GetNextPrime();
	}

	BOOST_TEST(p->GetNextPrime() == 541);
}

BOOST_AUTO_TEST_CASE(prime_sieve_get_prime_when_no_more_possible)
{
	PrimeSieve* p = new PrimeSieve(5);
	vector<int> result;
	for (int i = 0; i < 5; i++)
	{
		result.push_back(p->GetNextPrime());
	}
	vector<int> expect = {2, 3, 5, 5, 5};
	BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expect.begin(), expect.end());
}

BOOST_AUTO_TEST_SUITE_END()
#pragma endregion

#pragma region exercise3_test
BOOST_AUTO_TEST_SUITE(exercise3)

bool arrayIndexOutOfBoundsExceptionCheck(std::exception const& ex)
{
	BOOST_CHECK_EQUAL(ex.what(), std::string("ArrayIndexOutOfBoundsException"));
	return true;
}

bool indexOutOfBoundsExceptionCheck(std::exception const& ex)
{
	BOOST_CHECK_EQUAL(ex.what(), std::string("IndexOutOfBoundsException"));
	return true;
}

bool noSuchElementExceptionCheck(std::exception const& ex)
{
	BOOST_CHECK_EQUAL(ex.what(), std::string("NoSuchElementException"));
	return true;
}

BOOST_AUTO_TEST_CASE(kvector_array_index_out_of_bound_exception_when_insert_remove)
{
	KVector<int> v;
	v.add(2);
	v.add(3);
	v.add(4);
	BOOST_CHECK_EXCEPTION(v.insertElementAt(1, 5), std::exception, arrayIndexOutOfBoundsExceptionCheck);
	BOOST_CHECK_EXCEPTION(v.removeElementAt(5), std::exception, arrayIndexOutOfBoundsExceptionCheck);
}

BOOST_AUTO_TEST_CASE(kvector_index_out_of_bound_exception_when_call_lastindexof)
{
	KVector<int> v;
	v.add(2);
	v.add(3);
	v.add(4);
	BOOST_CHECK_EXCEPTION(v.lastIndexOf(1, 5), std::exception, indexOutOfBoundsExceptionCheck);
}

BOOST_AUTO_TEST_CASE(kvector_array_index_out_of_bound_exception_when_access_elementAt)
{
	KVector<int> v;
	v.add(2);
	v.add(3);
	v.add(4);
	BOOST_CHECK_EXCEPTION(v.elementAt(5), std::exception, arrayIndexOutOfBoundsExceptionCheck);
}

BOOST_AUTO_TEST_CASE(kvector_no_such_element_exception_when_get_first_or_last)
{
	KVector<int> v;
	BOOST_CHECK_EXCEPTION(v.firstElement(), std::exception, noSuchElementExceptionCheck);
	BOOST_CHECK_EXCEPTION(v.lastElement(), std::exception, noSuchElementExceptionCheck);
}

BOOST_AUTO_TEST_SUITE_END()
#pragma endregion

#pragma region exercise4_test
BOOST_AUTO_TEST_SUITE(exercise4)

bool badCastCheck(std::bad_cast const& ex)
{
	return true;
}

class Base
{
public:
	virtual void b() { cout << "b" << endl; }
};

class Derive1 : public Base
{
};

BOOST_AUTO_TEST_CASE(ptr_cast_downcast_base_to_derive1_success)
{
	Base* b = new Derive1();
	BOOST_REQUIRE_NO_THROW(ptr_cast<Derive1*>(b));
	Derive1* d = ptr_cast<Derive1*>(b);
	BOOST_TEST(d);

	Derive1 s;
	Base& b1 = s;
	BOOST_REQUIRE_NO_THROW(ptr_cast<Derive1&>(b1));
}

BOOST_AUTO_TEST_CASE(ptr_cast_downcast_base_to_derive1_fail)
{
	Base* b = new Base();
	BOOST_CHECK_EXCEPTION(ptr_cast<Derive1*>(b), std::bad_cast, badCastCheck);
	Base b1;
	BOOST_CHECK_EXCEPTION(ptr_cast<Derive1&>(b1), std::bad_cast, badCastCheck);
}

BOOST_AUTO_TEST_SUITE_END()
#pragma endregion

#pragma region exercise5_test
BOOST_AUTO_TEST_SUITE(exercise5)

BOOST_AUTO_TEST_CASE(exercise5)
{
	BOOST_TEST(true);
}

BOOST_AUTO_TEST_SUITE_END()
#pragma endregion

#pragma region exercise6_test
BOOST_AUTO_TEST_SUITE(exercise6)

BOOST_AUTO_TEST_CASE(exercise6)
{
	BOOST_TEST(true);
}

BOOST_AUTO_TEST_SUITE_END()
#pragma endregion

#pragma region exercise7_test
BOOST_AUTO_TEST_SUITE(exercise7)

BOOST_AUTO_TEST_CASE(date_lookup_success)
{
	string document = "line 0 3 a 2/12/2012 \n"
		"another 3-Aug-2012 08/12/45 \n"
		"end 2-07-2050 50-24-Jun \n";
	DateLookup date_lookup;
	auto result = date_lookup.Lookup(document);
	vector<string> expect0 = {"2/12/2012"};
	vector<string> expect1 = {"3-Aug-2012", "08/12/45" };
	vector<string> expect2 = {"2-07-2050", "50-24-Jun"};
	sort(expect1.begin(), expect1.end());
	sort(expect2.begin(), expect2.end());
	BOOST_TEST(result.size() == 4);
	BOOST_TEST(result[0] == expect0, boost::test_tools::per_element());
	BOOST_TEST(result[1] == expect1, boost::test_tools::per_element());
	BOOST_TEST(result[2] == expect2, boost::test_tools::per_element());
	BOOST_TEST(result[3].size() == 0);
}

BOOST_AUTO_TEST_CASE(date_lookup_empty_case)
{
	string document = "line 0 3 a 32/12/2012 \n"
		"another 3-Sdd-2012 08/32/45 \n"
		"end 11-a7-2050 50-55-Jun \n";
	DateLookup date_lookup;
	auto result = date_lookup.Lookup(document);
	vector<string> expect0 = { "2/12/2012" };
	vector<string> expect1 = { "3-Aug-2012", "08/12/45" };
	vector<string> expect2 = { "2-07-2050", "50-24-Jun" };
	sort(expect1.begin(), expect1.end());
	sort(expect2.begin(), expect2.end());
	BOOST_TEST(result.size() == 4);
	BOOST_TEST(result[0].size() == 0);
	BOOST_TEST(result[1].size() == 0);
	BOOST_TEST(result[2].size() == 0);
	BOOST_TEST(result[3].size() == 0);
}

BOOST_AUTO_TEST_SUITE_END()
#pragma endregion

#pragma region exercise8_test
BOOST_AUTO_TEST_SUITE(exercise8)

BOOST_AUTO_TEST_CASE(exercise8)
{
	BOOST_TEST(true);
}

BOOST_AUTO_TEST_SUITE_END()
#pragma endregion

