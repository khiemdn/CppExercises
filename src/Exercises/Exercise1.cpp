// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Exercise1.cpp" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the Exercise1.cpp
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#include "Exercise1.h"
#include <algorithm>

std::vector<int> MergeAndSort(const std::vector<int>& v1, const std::vector<int>& v2)
{
	std::vector<int> result;
	result.insert(result.begin(), v1.begin(), v1.end());
	result.insert(result.begin(), v2.begin(), v2.end());
	stable_sort(result.begin(), result.end());
	return result;
}

std::deque<int> RemoveEven(const std::deque<int>& d)
{
	std::deque<int> result(d);
	auto itend = remove_if(result.begin(), result.end(), [](int e) { return e % 2 == 0; });
	result.erase(itend, result.end());
	return result;
}

std::set<int> Intersection(const std::set<int>& s1, const std::set<int>& s2)
{
	std::vector<int> result(s1.size() + s2.size());
	auto itend = set_intersection(s1.cbegin(), s1.cend(), s2.cbegin(), s2.cend(), result.begin());
	result.resize(itend - result.begin());
	std::set<int> resultSet(result.begin(), result.end());
	return resultSet;
}
