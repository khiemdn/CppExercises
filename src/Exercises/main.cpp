// --------------------------------------------------------------------------------------------------------------------
// <copyright file="main.cpp" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the main.cpp
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#include <iostream>
#include <boost/algorithm/string.hpp>
#include "Exercise1.h"
#include "Exercise2.h"
#include "Exercise3.h"
#include "Exercise4.h"
#include "Exercise5.h"
#include "Exercise6.h"
#include "Exercise7.h"
#include "Exercise8.h"

using namespace std;

void exercise1run()
{
	vector<int> v1;
	try
	{
		PopulateInt<vector<int>>(v1, 50);
	}
	catch (const exception& e)
	{
		cout << e.what();
	}
	vector<int> v2;
	PopulateInt<vector<int>>(v2, 50);
	vector<int> v = MergeAndSort(v1, v2);

	deque<int> d;
	PopulateInt(d, 100);
	deque<int> r = RemoveEven(d);

	list<int> l;
	PopulateInt(l, 100);
	CountCriteria(l, [](int e) { return e > 80; });

	set<int> s1 = {3, 1, 23, 34, 6, 8, 65, 10, 7};
	set<int> s2 = {23, 2, 12, 32, 2, 1, 8, 2, 6};
	set<int> s = Intersection(s1, s2);

	map<string, int> m1;
	m1["a"] = 2;
	m1["b"] = 3;
	m1["c"] = 4;
	m1["d"] = 5;
	map<string, int> m = RemoveCriteria(m1, [](const pair<string, int>& e) { return e.second > 2; });

	for (auto it = m.cbegin(); it != m.cend(); ++it)
	{
		cout << it->first << " " << it->second << endl;
	}
}

void exercise2run()
{
	PrimeSieve p(100);
	for (int i = 0; i < 50; i++)
		cout << p.GetNextPrime() << " ";
	cout << endl;
}

void exercise7run()
{
	string document = "line 0 3 a 2/12/2012 2030/14/02 \n"
		"waow \n"
		"new 12-Jul-2012 Aug-09-50\n"
		"another 3-Aug-2012 08/12/45\n"
		"end 2-07-2050 50-24-Jun\n";
	DateLookup dateLookup;

	auto r = dateLookup.Lookup(document, "2050/24/12");

	for (auto i : r)
	{
		if (i.second.size() > 0) {
			cout << i.first << ": ";
			for (auto d : i.second)
			{
				cout << d << " ";
			}
			cout << endl;
		}
	}
}

class Base
{
public:
	virtual void b() { cout << "b" << endl; }
};

class Derive1 : public Base
{
};

void exercise4run()
{
	Base* b = new Base();
	try {
		Derive1* d1 = ptr_cast<Derive1*>(b);
	} catch(bad_cast&)
	{
		cout << "bad cast";
	}
}

int main(int argc, char* argv[])
{
	cout << "Exercises: " << endl;

	//exercise1run();
	//exercise2run();
	//exercise4run();
	//exercise7run();

	{
		KPtr<int> p(new int(2));
//		KPtr<int> p2 = p;
	}


	const char* ct = " sad dasd";
	list<string> vs = {"dude", "sad", "daas"};

	//print(vs);

	Pool_alloc<int> ml;
	int* p = ml.allocate();
	ml.construct(p, 0, 2);
	ml.construct(p, 3, 3);
	ml.construct(p, 6, 1);
	ml.construct(p, 16, 1);

	int pause;
	cin >> pause;

	return 1;
}
