// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Exercise1.h" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the Exercise1.h
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma once
#include <vector>
#include <deque>
#include <list>
#include <set>
#include <map>
#include <string>
#include <algorithm>
#include <functional>

std::vector<int> MergeAndSort(const std::vector<int>& v1, const std::vector<int>& v2);

std::deque<int> RemoveEven(const std::deque<int>& d);

template <class Func>
int CountCriteria(const std::list<int>& l, Func pred)
{
	int result = std::count_if(l.cbegin(), l.cend(), pred);
	return result;
}

std::set<int> Intersection(const std::set<int>& s1, const std::set<int>& s2);

template <typename Pred>
std::map<std::string, int> RemoveCriteria(const std::map<std::string, int>& m, Pred& pred);

template <class T>
void PopulateInt(T& container, int range)
{
	try
	{
		container.resize(range);
		std::generate_n(container.begin(), range, []() { return rand() % 100; });
	}
	catch (const std::exception&)
	{
		throw std::exception("Cant populate container");
	}
}

template <typename Pred>
std::map<std::string, int> RemoveCriteria(const std::map<std::string, int>& m, Pred& pred)
{
	try
	{
		std::map<std::string, int> result;
		for (auto it = m.cbegin(); it != m.cend(); ++it)
		{
			if (pred(*it)) 
			{
				result.insert(*it);
			}
		}
		return result;
	}
	catch (const std::exception&)
	{
		throw std::exception("Error");
	}
}
