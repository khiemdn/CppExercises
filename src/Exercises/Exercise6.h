// ***********************************************************************
// Assembly         : Exercises
// Author           : KhiemDN
// Created          : 08-30-2017
// Last Modified By : KhiemDN
// Last Modified On : 09-05-2017  6:51 AM
// ***********************************************************************
// <copyright file="Exercise6.h" company="VNG Corporation">
//   Copyright (c) 2017 VNG Corporation. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

#pragma once
#include <cstdlib>
#include <iostream>

template <class T>
class Pool_alloc
{
public:
	const int POOL_SIZE = 10;

	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T value_type;

	template <class U>
	struct rebind
	{
		typedef Pool_alloc<U> other;
	};

	Pool_alloc()
	{
	};

	Pool_alloc(const Pool_alloc&)
	{
	};

	template <class U>
	Pool_alloc(const Pool_alloc<U>&)
	{
	};
	int current_size = 0;
	pointer init_pointer = nullptr;

	static pointer address(reference x) { return &x; }
	static const_pointer address(const_reference x) { return &x; }
	static size_type max_size() { return size_t(-1); }
	Pool_alloc<T>& operator=(const Pool_alloc&) { return *this; }
	static void destroy(pointer p) { p->~T(); }

	void construct(pointer& ptr, int pos, const value_type& t)
	{
		if (pos >= current_size)
		{
			pointer newP = reAllocate(ceil((float) pos / (float) POOL_SIZE));
			ptr = newP;
		}
		
		if (pos >= 0)
		{
			pointer tmp = ptr;
			tmp = tmp + pos;
			new(tmp) T(t);
		}
	}

	pointer allocate()
	{
		T* t = static_cast<T*>(malloc(POOL_SIZE * sizeof(T)));
		current_size = POOL_SIZE;
		init_pointer = t;
		return t;
	}

	pointer reAllocate(int f)
	{
		T* t = static_cast<T*>(malloc(POOL_SIZE * f * sizeof(T)));
		memcpy(t, init_pointer, current_size * sizeof(T));
		current_size = POOL_SIZE * f;
		init_pointer = t;
		return t;
	}

	static void deallocate(void* p, size_type)
	{
		if (p)
		{
			free(p);
		}
	}
};
