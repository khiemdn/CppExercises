#pragma once
#include <string>
#include <regex>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>

class DateLookup
{
public:
	// use boost instead of regex which support look behind
	std::map<int, std::vector<std::string>> Lookup(std::string document, std::string excludeFormat = "");
private:
	const std::vector<std::string> PATTERNS = {
		"(?<!\\d)(0?[1-9]|1[0-9]|2[0-9]|3[0-1])/(0?[1-9]|1[0-2])/(20\\d{2})(?!\\d)",									// dd/mm/yyyy
		"(?<!\\d)(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(0?[1-9]|1[0-2])-(20\\d{2})(?!\\d)",									// dd-mm-yyyy
		"(?<!\\d)(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(20\\d{2})(?!\\d)",	// dd-mm-yyyy
		"(?<!\\d)(0?[1-9]|1[0-9]|2[0-9]|3[0-1])/(0?[1-9]|1[0-2])/(\\d{2})(?!\\d)",										// dd/mm/yy
		"(?<!\\d)(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(0?[1-9]|1[0-2])-(\\d{2})(?!\\d)",										// dd-mm-yy
		"(?<!\\d)(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(\\d{2})(?!\\d)",		// dd-mm-yy
		"(?<!\\d)(0?[1-9]|1[0-2])/(0?[1-9]|1[0-9]|2[0-9]|3[0-1])/(20\\d{2})(?!\\d)",									// mm/dd/yyyy
		"(?<!\\d)(0?[1-9]|1[0-2])-(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(20\\d{2})(?!\\d)",									// mm-dd-yyyy
		"(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(20\\d{2})(?!\\d)",			// mm-dd-yyyy
		"(?<!\\d)(0?[1-9]|1[0-2])/(0?[1-9]|1[0-9]|2[0-9]|3[0-1])/(\\d{2})(?!\\d)",										// mm/dd/yy
		"(?<!\\d)(0?[1-9]|1[0-2])-(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(\\d{2})(?!\\d)",										// mm-dd-yy
		"(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(\\d{2})(?!\\d)",				// mm-dd-yy
		"(?<!\\d)(20\\d{2})/(0?[1-9]|1[0-9]|2[0-9]|3[0-1])/(0?[1-9]|1[0-2])(?!\\d)",									// yyyy/dd/mm
		"(?<!\\d)(20\\d{2})-(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(0?[1-9]|1[0-2])(?!\\d)",									// yyyy-dd-mm
		"(?<!\\d)(20\\d{2})-(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)",			// yyyy-dd-mm
		"(?<!\\d)(\\d{2})/(0?[1-9]|1[0-9]|2[0-9]|3[0-1])/(0?[1-9]|1[0-2])(?!\\d)",										// yy/dd/mm
		"(?<!\\d)(\\d{2})-(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(0?[1-9]|1[0-2])(?!\\d)",										// yy-dd-mm
		"(?<!\\d)(\\d{2})-(0?[1-9]|1[0-9]|2[0-9]|3[0-1])-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)",			// yy-dd-mm
	};
};