#pragma once

template<typename T>
class KPtr
{
public:
	KPtr(T* p) : _p(p)
	{
	}
	~KPtr()
	{
		delete _p;
	}
	T & operator*()
	{
		return *_p;
	}
	T* operator -> ()
	{
		return _p;
	}
private:
	T* _p;
};