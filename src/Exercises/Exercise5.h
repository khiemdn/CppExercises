#pragma once
#include <list>
#include <string>
#include <boost/typeof/typeof.hpp>
#include <type_traits>

void printCStr(const char* c_str)
{
	printf(c_str);
}

void printListStr(std::list<std::string> l)
{
	if (l.size() <= 0)
	{
		return;
	}
	for (auto it = l.cbegin(); it != l.cend(); ++it)
	{
		std::cout << *it;
	}
}