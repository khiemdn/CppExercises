// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Exercise4.h" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the Exercise4.h
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma once

template <typename To, typename Ty>
To ptr_cast(Ty* ty)
{
	try
	{
		To t = dynamic_cast<To>(ty);
		if (t)
		{
			return t;
		}
		throw std::bad_cast();
	}
	catch (std::bad_cast&)
	{
		throw std::bad_cast();
	}
}

template <typename To, typename Ty>
To& ptr_cast(Ty& ty)
{
	try
	{
		To& t = dynamic_cast<To&>(ty);
		return t;
	}
	catch (std::bad_cast&)
	{
		throw std::bad_cast();
	}
}