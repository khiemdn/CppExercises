// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Exercise2.h" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the Exercise2.h
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma once
#include <boost/coroutine2/all.hpp>
#include <vector>

typedef boost::coroutines2::coroutine<int> coro_t;

class PrimeSieve
{
public:
	/**
	 * \brief init
	 * \param maxNumber : all prime numbers generated are less than this
	 */
	PrimeSieve(int maxNumber);
	~PrimeSieve();

	/**
	 * \brief get the next prime number
	 * \return prime number
	 */
	int GetNextPrime();

private:
	std::vector<coro_t::pull_type*> _coroutines;
	int _maxNumber;
	int _currentPrime;

	/**
	 * \brief create a pointer to a new coroutine that filter all numbers in the source coroutine which can be devided by prime
	 * \param source input coroutin
	 * \param prime input prime number
	 * \return 
	 */
	coro_t::pull_type* GenerateCoroutine(coro_t::pull_type& source, int prime);
};

//coro_t::pull_type* gen(coro_t::pull_type& source, int prime)
//{
//	// prime must be pass by value or it becomes undefined after first loop
//	coro_t::pull_type* newSource = new coro_t::pull_type([&, prime](coro_t::push_type& sink)
//	{
//		while (source)
//		{
//			int i = source.get();
//			if (i % prime != 0)
//			{
//				sink(i);
//			}
//			source();
//		}
//	});
//	return newSource;
//}

//void test()
//{
//	std::vector<coro_t::pull_type*> cs;
//	coro_t::pull_type* beginSource = new coro_t::pull_type([&](coro_t::push_type& sink)
//	{
//		for (int i = 2; i < 1000; i++)
//		{
//			sink(i);
//		}
//	});
//	cs.push_back(beginSource);
//	
//	for (int i =0; i<100; i++)
//	{
//		int prime = cs.back()->get(); std::cout << prime << " ";
//		cs.back()->operator()();
//		coro_t::pull_type* source = gen(*cs.back() , prime);
//		cs.push_back(source);
//	}
//}
