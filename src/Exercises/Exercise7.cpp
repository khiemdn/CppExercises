#include "Exercise7.h"

std::map<int, std::vector<std::string>> DateLookup::Lookup(std::string document, std::string excludeFormat)
{
	//seperate into lines
	std::map<int, std::vector<std::string>> result;
	std::vector<std::string> lines;
	boost::algorithm::split(lines, document, boost::is_any_of("\n"), boost::token_compress_on);

	std::string excludePattern = "";

	if (excludeFormat != "")
	{
		for (auto pattern = PATTERNS.cbegin(); pattern != PATTERNS.cend(); ++pattern)
		{
			boost::regex datePattern(*pattern, std::regex_constants::icase);
			if (boost::regex_match(excludeFormat, datePattern))
			{
				excludePattern = *pattern;
				break;
			}
		}
	}
	boost::regex excludeRegex(excludePattern);

	for (unsigned int i = 0; i < lines.size(); ++i)
	{
		// apply pattern to each line
		for (auto pattern = PATTERNS.cbegin(); pattern != PATTERNS.cend(); ++pattern)
		{
			std::string input = lines[i];
			boost::regex dateRegex(*pattern, std::regex_constants::icase);
			boost::sregex_iterator it(input.begin(), input.end(), dateRegex);
			boost::sregex_iterator it_end = boost::sregex_iterator();
			while (it != it_end) {
				boost::smatch match = *it;
				if (!boost::regex_match(match.str(), excludeRegex)) {
					result[i].push_back(match.str());
				}
				++it;
			}
		}
		std::sort(result[i].begin(), result[i].end());
		result[i].erase(std::unique(result[i].begin(), result[i].end()), result[i].end());
	}
	return result;
}
