#include "Exercise2.h"
#include <iostream>

PrimeSieve::PrimeSieve(int maxNumber) : _currentPrime(0)
{
	if (maxNumber < 2)
	{
		maxNumber = 2;
	}
	_maxNumber = maxNumber;
	coro_t::pull_type* beginSource = new coro_t::pull_type([&, maxNumber](coro_t::push_type& sink)
	{
		for (int i = 2; i <= maxNumber; i++)
		{
			sink(i);
		}
	});
	_coroutines.clear();
	_coroutines.push_back(beginSource);
}

PrimeSieve::~PrimeSieve()
{
}

int PrimeSieve::GetNextPrime()
{
	if (*_coroutines.back()) {
		_currentPrime = _coroutines.back()->get();
	} else
	{
		return _currentPrime;
	}
	_coroutines.back()->operator()();
	coro_t::pull_type* source = GenerateCoroutine(*_coroutines.back(), _currentPrime);
	_coroutines.push_back(source);
	return _currentPrime;
}

coro_t::pull_type* PrimeSieve::GenerateCoroutine(coro_t::pull_type& source, int prime)
{
	// prime must be pass by value or it becomes undefined after first loop
	coro_t::pull_type* newSource = new coro_t::pull_type([&, prime](coro_t::push_type& sink)
	{
		while (source)
		{
			int i = source.get();
			if (i % prime != 0)
			{
				sink(i);
			}
			source();
		}
	});
	return newSource;
}

