// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Exercise3.h" company="VNG">
// 
// </copyright>
// <summary>
//   Defines the Exercise3.h
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#pragma once
#include <exception>
#include <array>

template <class T>
class KVector
{
public:
	KVector();
	KVector(int initialCapacity);
	KVector(int initialCapacity, int capacityIncrement); // throw IllegalArgumentException
	~KVector();
	bool add(T o);
	int lastIndexOf(T o, int index); // throw IndexOutOfBoundsException
	T elementAt(int index); // throw ArrayIndexOutOfBoundsException
	T firstElement(); // throw NoSuchElementException
	T lastElement(); // throw NoSuchElementException
	void setElementAt(T o, int index); // throw ArrayIndexOutOfBoundsException
	void removeElementAt(int index); // throw ArrayIndexOutOfBoundsException
	void insertElementAt(T o, int index); // throw ArrayIndexOutOfBoundsException
	T get(int index); // throw ArrayIndexOutOfBoundsException
	T set(int index, T o); // throw ArrayIndexOutOfBoundsException
	T remove(int index); // throw ArrayIndexOutOfBoundsException
	bool addAll(int index, KVector<T> collection); //throw ArrayIndexOutOfBoundsException
	void ensureCapacity(int minCapacity);
	int getCapacity() const;
	void clear();
	
private:
	int capacityIncrement;
	int elementCount;
	int capacity;
	T* elementData;
	static const int MAX_ARRAY_SIZE = INT_MAX - 8;

	void grow(int minCapacity);
	static int hugeCapacity(int minCapacity); // throw OutOfMemoryError
};

template <class T>
KVector<T>::KVector() : KVector(10)
{
}

template <class T>
KVector<T>::KVector(int initialCapacity) : KVector(initialCapacity, 0)
{
}

template <class T>
KVector<T>::KVector(int initialCapacity, int capacityIncrement)
{
	if (initialCapacity < 0)
	{
		throw std::exception("IllegalArgumentException");
	}
	this->capacityIncrement = capacityIncrement;
	this->elementData = new T[initialCapacity];
	capacity = initialCapacity;
	for (int i = 0; i < initialCapacity; i++)
	{
		elementData[i] = NULL;
	}
	elementCount = 0;
}

template <class T>
KVector<T>::~KVector()
{
	for (int i = 0; i < elementCount; i++)
	{
		elementData[i] = NULL;
	}
	elementCount = 0;
}

template <class T>
bool KVector<T>::add(T o)
{
	ensureCapacity(elementCount + 1);
	elementData[elementCount++] = o;
	return true;
}

template <class T>
int KVector<T>::lastIndexOf(T o, int index)
{
	if (index >= elementCount)
	{
		throw std::exception("IndexOutOfBoundsException");
	}
	if (o == NULL)
	{
		for (int i=index; i>=0; i--)
		{
			if (elementData[index] == NULL)
			{
				return i;
			}
		}
	}
	else
	{
		for (int i = index; i >= 0; i--)
		{
			if (elementData[index] == o)
			{
				return i;
			}
		}
	}
	return -1;
}

template <class T>
T KVector<T>::elementAt(int index)
{
	if (index >= elementCount)
	{
		throw std::exception("ArrayIndexOutOfBoundsException");
	}
	return (T) elementData[index];
}

template <class T>
T KVector<T>::firstElement()
{
	if (elementCount <= 0)
	{
		throw std::exception("NoSuchElementException");
	}
	return (T) elementData[0];
}

template <class T>
T KVector<T>::lastElement()
{
	if (elementCount <= 0)
	{
		throw std::exception("NoSuchElementException");
	}
	return (T)elementData[elementCount - 1];
}

template <class T>
void KVector<T>::setElementAt(T o, int index)
{
	if (index >= elementCount)
	{
		throw std::exception("ArrayIndexOutOfBoundsException");
	}
	elementData[index] = o;
}

template <class T>
void KVector<T>::removeElementAt(int index)
{
	if (index >= elementCount)
	{
		throw std::exception("ArrayIndexOutOfBoundsException");
	} else if (index < 0)
	{
		throw std::exception("ArrayIndexOutOfBoundsException");
	}
	int j = elementCount - index - 1;
	memcpy(elementData + index, elementData + index + 1, sizeof(T)*j);
	elementCount--;
	elementData[elementCount] = NULL;
}

template <class T>
void KVector<T>::insertElementAt(T o, int index)
{
	if (index > elementCount)
	{
		throw std::exception("ArrayIndexOutOfBoundsException");
	}
	ensureCapacity(elementCount + 1);
	memcpy(elementData + index + 1, elementData + index, sizeof(T)*(elementCount - index));
	elementData[index] = o;
	elementCount++;
}

template <class T>
T KVector<T>::get(int index)
{
	if (index >= elementCount)
	{
		throw std::exception("ArrayIndexOutOfBoundsException");
	}
	return (T) elementData[index];
}

template <class T>
T KVector<T>::set(int index, T o)
{
	if (index >= elementCount)
	{
		throw std::exception("ArrayIndexOutOfBoundsException");
	}
	T oldValue = elementData[index];
	elementData[index] = o;
	return oldValue;
}

template <class T>
T KVector<T>::remove(int index)
{
	if (index >= elementCount)
	{
		throw std::exception("ArrayIndexOutOfBoundsException");
	}
	T oldValue = elementData[index];
	int numMoved = elementCount - index - 1;
	memcpy(elementData + index, elementData + index + 1, sizeof(T)*numMoved);
	return oldValue;
}

template <class T>
bool KVector<T>::addAll(int index, KVector<T> collection)
{
}

template <class T>
void KVector<T>::ensureCapacity(int minCapacity)
{
	if (minCapacity - capacity > 0)
	{
		grow(minCapacity);
	}
}

template <class T>
int KVector<T>::getCapacity() const
{
	return capacity;
}

template <class T>
void KVector<T>::clear()
{
	for(int i =0; i < elementCount; i++)
	{
		elementData[i] = NULL;
	}
	elementCount = 0;
}

template <class T>
void KVector<T>::grow(int minCapacity)
{
	int oldCapacity = capacity;
	int newCapacity = oldCapacity + (capacityIncrement > 0 ? capacityIncrement : oldCapacity);
	if (newCapacity - minCapacity < 0)
	{
		newCapacity = minCapacity;
	}
	
	if(newCapacity - MAX_ARRAY_SIZE > 0)
	{
		newCapacity = hugeCapacity(minCapacity);
	}
	T* newData = new T[newCapacity];
	memcpy(newData, elementData, sizeof(T)*elementCount);
	for (int i = 0; i < elementCount; i++)
	{
		elementData[i] = NULL;
	}
	elementData = newData;
	capacity = newCapacity;
}

template <class T>
int KVector<T>::hugeCapacity(int minCapacity)
{
	if (minCapacity < 0)
	{
		throw std::exception("OutOfMemoryError");
	}
	return (minCapacity > MAX_ARRAY_SIZE ? INT_MAX : MAX_ARRAY_SIZE);
}
